package com.example.apngview;

import java.io.File;

import android.app.Activity;
import android.net.Uri;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import com.nostra13.universalimageloader.cache.memory.impl.LruMemoryCache;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;

public class MainActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
		FLog.d("Initialize Universal Image Loader");
		ImageLoaderConfiguration configuration = 
				new ImageLoaderConfiguration.Builder(getApplicationContext())
						.memoryCache(new LruMemoryCache(2 * 1024 * 1024))
				        .memoryCacheSize(2 * 1024 * 1024)
				        .diskCacheSize(50 * 1024 * 1024)
				        .diskCacheFileCount(100)
						.build();
		
		ImageLoader.getInstance().init(configuration);
		
		String filename = "55F7AAB0AB28AA3F89E5B27F4ED08ECF.png";
		
		File external = getExternalFilesDir(null);
		
		File workingDir = new File(external, "apng");
		FLog.d("workingDir: %s", workingDir);
		
		File apng = new File(workingDir, filename);
		
		if (!workingDir.exists()) {
			workingDir.mkdirs();
		}
		
		if (!apng.exists()) {
			FLog.d("Copy APNG");
			AssetHelper.extractAsset(getAssets(), "apng", workingDir.getPath(), filename);
			FLog.d("Copy Success");
		}
		
		try {
			final ApngView apngView = (ApngView) findViewById(R.id.image_apng);
			apngView.setImageURI(Uri.fromFile(apng));
			apngView.setNumPlays(3);
			
			apngView.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					if (apngView.isRunning()) {
						apngView.stop();
					} else {
						apngView.start();
					}
				}
			});
			
		} catch (Exception e) {
			FLog.w("Error: %s", e.toString());
		}
	}
	
	@Override
	protected void onResume() {
		super.onResume();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
}
