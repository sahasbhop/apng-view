package com.example.apngview;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;

import android.app.Activity;
import android.content.res.AssetManager;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Bitmap.CompressFormat;
import android.graphics.Bitmap.Config;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.PorterDuff.Mode;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.drawable.AnimationDrawable;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.media.ExifInterface;
import android.util.DisplayMetrics;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

public class BitmapUtil {

	private static final CompressFormat DEFAULT_COMPRESS_FORMAT = CompressFormat.PNG;
	private static final int BITMAP_COMPRESS_QUALITY = 80;
	
	public static DisplayMetrics getDisplayMetrics(Activity activity) {
		DisplayMetrics metrics = new DisplayMetrics();
		activity.getWindowManager().getDefaultDisplay().getMetrics(metrics);
		return metrics;
	}
	
	public static Bitmap getBitmap(Resources res, int resId) {
		return BitmapFactory.decodeResource(res, resId, null);
	}
	
	public static Bitmap getBitmap(
			Resources res, int resId, int width, int height) {
	
		BitmapFactory.Options options = new BitmapFactory.Options();
		options.inJustDecodeBounds = true;
		
		BitmapFactory.decodeResource(res, resId, options);
	
		options.inSampleSize = calculateInSampleSize(options, width, height);
		options.inJustDecodeBounds = false;
		
		return BitmapFactory.decodeResource(res, resId, options);
	}

	public static Bitmap getBitmap(AssetManager asset, String fileName) {
		Bitmap bitmap = null;
		
		try {
			InputStream is = asset.open(fileName);
			bitmap = BitmapFactory.decodeStream(is);
	
		} catch (IOException e) {
			FLog.e("Asset error: %s", e.toString());
		}
	
		return bitmap;
	}
	
	public static Bitmap getBitmap(InputStream input, int width, int height) {
		Bitmap bitmap = null;
		
		byte[] byteArr = new byte[0];
        byte[] buffer = new byte[1024];
        int len = 0;
        int count = 0;
		
	    try {
            while ((len = input.read(buffer)) > -1) {
                if (len != 0) {
                    if (count + len > byteArr.length) {
                        byte[] newbuf = new byte[(count + len) * 2];
                        System.arraycopy(byteArr, 0, newbuf, 0, count);
                        byteArr = newbuf;
                    }

                    System.arraycopy(buffer, 0, byteArr, count, len);
                    count += len;
                }
            }
	            
	        //Decode image size
	        BitmapFactory.Options options = new BitmapFactory.Options();
	        options.inJustDecodeBounds = true;
	        
	        BitmapFactory.decodeByteArray(byteArr, 0, count, options);
	        
	        options.inSampleSize= calculateInSampleSize(options, width, height);
	        options.inJustDecodeBounds = false;
	        
	        bitmap = BitmapFactory.decodeByteArray(byteArr, 0, count, options);
	        
	    } catch (Exception e) {
	    	FLog.e("Error %s", e.toString());
	    }
	    
	    return bitmap;
	}
	
	public static Bitmap getBitmap(String filePath) {
		Bitmap bitmap = null;
		
	    try {
	        bitmap = BitmapFactory.decodeFile(filePath, null);
	    } catch (Exception e) {
	    	FLog.e("Error %s", e.toString());
	    }
	    
	    return bitmap;
	}

	public static Bitmap getBitmap(String filePath, int width, int height){
		Bitmap bitmap = null;
		
	    try {
	        //Decode image size
	        BitmapFactory.Options options = new BitmapFactory.Options();
	        options.inJustDecodeBounds = true;
	        
	        BitmapFactory.decodeFile(filePath, options);
	        
	        options.inSampleSize= calculateInSampleSize(options, width, height);
	        options.inJustDecodeBounds = false;
	        
	        bitmap = BitmapFactory.decodeFile(filePath, options);
	        
	    } catch (Exception e) {
	    	FLog.e("Error %s", e.toString());
	    }
	    
	    return bitmap;
	}

	public static Bitmap rotate(Bitmap bitmap, int rotation, int width, int height){
		if (bitmap != null) {
			Matrix matrix = new Matrix();
			matrix.postRotate(rotation);
	
			height = bitmap.getHeight() < height? bitmap.getHeight(): height;
			width = bitmap.getWidth() < width? bitmap.getWidth(): width;
			
			return Bitmap.createBitmap(bitmap, 0, 0, width, height, matrix, false);
		}
		return null;
	}
	
	public static Bitmap shrinkBitmap(String file, int width, int height) {
		if( file == null){
			return null;
		}
		BitmapFactory.Options bmpFactoryOptions = new BitmapFactory.Options();
		bmpFactoryOptions.inJustDecodeBounds = true;
		Bitmap bitmap = BitmapFactory.decodeFile(file, bmpFactoryOptions);

		int heightRatio = (int) Math.ceil(bmpFactoryOptions.outHeight
				/ (float) height);
		int widthRatio = (int) Math.ceil(bmpFactoryOptions.outWidth
				/ (float) width);

		if (heightRatio > 1 || widthRatio > 1) {
			if (heightRatio > widthRatio) {
				bmpFactoryOptions.inSampleSize = heightRatio;
			} else {
				bmpFactoryOptions.inSampleSize = widthRatio;
			}
		}

		bmpFactoryOptions.inJustDecodeBounds = false;
		bitmap = BitmapFactory.decodeFile(file, bmpFactoryOptions);
		return bitmap;
	}

	public static Bitmap scaleDown(Bitmap realImage, int maxImageSize,
	        boolean filter) {
		if (realImage == null) {
			return null;
		}
		
	    float ratio = Math.min(
	            (float) maxImageSize / realImage.getWidth(),
	            (float) maxImageSize / realImage.getHeight());
	    int width = Math.round((float) ratio * realImage.getWidth());
	    int height = Math.round((float) ratio * realImage.getHeight());

	    Bitmap newBitmap = Bitmap.createScaledBitmap(realImage, width,
	            height, filter);
	    return newBitmap;
	}
	
	public static int getOrientation(File imageFile) {
		ExifInterface exif;
		int orientation = 0;
		int rotate = 0;
		try {
			exif = new ExifInterface(imageFile.getAbsolutePath());
			orientation = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_NORMAL);
			 switch (orientation) {
		        case ExifInterface.ORIENTATION_ROTATE_270:
		            rotate = 270;
		            break;
		        case ExifInterface.ORIENTATION_ROTATE_180:
		            rotate = 180;
		            break;
		        case ExifInterface.ORIENTATION_ROTATE_90:
		            rotate = 90;
		            break;
		        }
		} catch (IOException e) {
			FLog.e("Error: %s", e.toString());
		}
		return rotate;
	}
	
	public static void recycleBitmap(ImageView imageView) {
		if (imageView != null) {
			Drawable drawable = imageView.getDrawable();

			imageView.setImageBitmap(null);
			imageView.setImageDrawable(null);

			if (drawable != null) {
				try {
					recycleDrawable(drawable);
				} catch (Exception e) {
					FLog.e("Asset error: %s", e.toString());
				}
			}
		}
	}

	@SuppressWarnings("deprecation")
	public static void recycleDrawable(LinearLayout linearLayout) {
		Drawable drawable = linearLayout.getBackground();
		try {
			recycleDrawable(drawable);
		} catch (Exception e) {
			FLog.e("Asset error: %s", e.toString());
		}
		linearLayout.setBackgroundDrawable(null);
	}

	@SuppressWarnings("deprecation")
	public static void recycleDrawable(RelativeLayout relativeLayout) {
		Drawable drawable = relativeLayout.getBackground();
		try {
			recycleDrawable(drawable);
		} catch (Exception e) {
			FLog.e("Asset error: %s", e.toString());
		}
		relativeLayout.setBackgroundDrawable(null);
	}

	public static void recycleDrawable(Drawable drawable) {
		if (drawable != null) {
			drawable.setCallback(null);
		}

		if (drawable instanceof BitmapDrawable) {
			Bitmap bitmap = ((BitmapDrawable) drawable).getBitmap();

			if (bitmap != null) {
				bitmap.recycle();
				bitmap = null;
			}

		} else if (drawable instanceof AnimationDrawable) {
			AnimationDrawable anime = (AnimationDrawable) drawable;

			if (anime.isRunning()) {
				anime.stop();
			}

			int frameCount = anime.getNumberOfFrames();

			Drawable frame = null;
			Bitmap bitmap = null;

			for (int i = 0; i < frameCount; i++) {
				frame = anime.getFrame(i);

				if (frame instanceof BitmapDrawable) {
					bitmap = ((BitmapDrawable) frame).getBitmap();

					if (bitmap != null) {
						bitmap.recycle();
						bitmap = null;
					}
				}
			}
		}
	}

	public static byte[] getByteArray(Bitmap bitmap, CompressFormat format) {
		ByteArrayOutputStream stream = new ByteArrayOutputStream();
		bitmap.compress(format, 100, stream);
		return stream.toByteArray();
	}

	public static Bitmap addWaterMark(Bitmap image, Bitmap watermark,
			int width, int height) {

		Bitmap bitmap = Bitmap.createBitmap(width, height,
				Bitmap.Config.ARGB_8888);

		Canvas canvas = new Canvas(bitmap);
		canvas.drawBitmap(image, 0f, 0f, null);
		canvas.drawBitmap(watermark, 0f, 0f, null);

		return bitmap;
	}
	
	public static int calculateInSampleSize(
			BitmapFactory.Options options, int reqWidth, int reqHeight) {
		
        // Raw height and width of image
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;

        if (height > reqHeight || width > reqWidth) {

            // Calculate ratios of height and width to requested height and width
            final int heightRatio = Math.round((float) height / (float) reqHeight);
            final int widthRatio = Math.round((float) width / (float) reqWidth);

            // Choose the smallest ratio as inSampleSize value, this will guarantee a final image
            // with both dimensions larger than or equal to the requested height and width.
            inSampleSize = heightRatio < widthRatio ? heightRatio : widthRatio;

            // This offers some additional logic in case the image has a strange
            // aspect ratio. For example, a panorama may have a much larger
            // width than height. In these cases the total pixels might still
            // end up being too large to fit comfortably in memory, so we should
            // be more aggressive with sample down the image (=larger inSampleSize).

            final float totalPixels = width * height;

            // Anything more than 2x the requested pixels we'll sample down further
            final float totalReqPixelsCap = reqWidth * reqHeight * 2;

            while (totalPixels / (inSampleSize * inSampleSize) > totalReqPixelsCap) {
                inSampleSize++;
            }
        }
        return inSampleSize;
    }
	
	public static Bitmap getCircularShape(Bitmap bitmap) {
		Bitmap output = Bitmap.createBitmap(
				bitmap.getWidth(), bitmap.getHeight(), Config.ARGB_8888);
		
		Canvas canvas = new Canvas(output);

		final int color = 0xff424242;
		final Paint paint = new Paint();
		final Rect rect = new Rect(0, 0, bitmap.getWidth(), bitmap.getHeight());
		
		paint.setAntiAlias(true);
		
		canvas.drawARGB(0, 0, 0, 0);
		
		paint.setColor(color);
		
		canvas.drawCircle(
				bitmap.getWidth() / 2, 
				bitmap.getHeight() / 2, 
				bitmap.getWidth() / 2, 
				paint);
		
		paint.setXfermode(new PorterDuffXfermode(Mode.SRC_IN));
		canvas.drawBitmap(bitmap, rect, rect, paint);

		if (bitmap != null) {
			bitmap.recycle();
			System.gc();
			bitmap = null;
		}
		return output;
	}
	
}
