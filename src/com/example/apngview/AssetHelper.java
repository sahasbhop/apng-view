package com.example.apngview;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;

import android.content.res.AssetManager;

public class AssetHelper {
	
	public static boolean isAssetAvailable(AssetManager am, String fileName) {
		boolean isAvailable = false;
		
		try {
			InputStream is = am.open(fileName);
			is.close();
			isAvailable = true;
		} catch (IOException e) { /* ignore */ }
		
		return isAvailable;
	}

	public static void extractAssets(
			AssetManager assetManager, String assetsDir, String targetDir) {
		
		FLog.v("ENTER ...");
		
		String[] fileList = null;
		
		try {
			fileList = assetManager.list(assetsDir);
		}
		catch (Exception e) {
			FLog.e("Error: %s", e.toString());
			return;
		}
		
		for (String filename : fileList) {
			extractAsset(assetManager, assetsDir, targetDir, filename);
		}
		
		FLog.v("EXIT ...");
	}

	public static void extractAsset(AssetManager assetManager, 
			String assetDir, String targetDir, String filename) {
		
		String assetPath = String.format("%s/%s", assetDir, filename);
		String targetPath = String.format("%s/%s", targetDir, filename);
		
		File f = new File(targetPath);
		
		if (! f.exists()) {
			FLog.v("Extract '%s' to '%s'", assetPath, targetPath);
			InputStream in = null;
			FileOutputStream out = null;
			
			try {
				in = assetManager.open(assetPath);
				out = new FileOutputStream(f, false);
				
				byte buffer[] = new byte[in.available()];
				in.read(buffer);
				
				out.write(buffer);
				out.flush();
			}
			catch (Exception e) {
				FLog.e("Error: %s", e.toString());
			}
			finally {
				if (in != null) {
					try { in.close(); }
					catch (IOException e) { /* ignore */ }
				}
				if (out != null) {
					try { out.close(); }
					catch (IOException e) { /* ignore */ }
				}
			} // End try-catch
		} // End if a file is not existed
	}
	
}
