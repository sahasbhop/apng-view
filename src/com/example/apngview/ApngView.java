package com.example.apngview;

import java.io.File;
import java.util.ArrayList;

import org.apache.commons.io.FileUtils;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.RectF;
import android.graphics.drawable.Animatable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.util.AttributeSet;
import android.widget.ImageView;
import ar.com.hjg.pngj.PngReaderApng;
import ar.com.hjg.pngj.chunks.PngChunk;
import ar.com.hjg.pngj.chunks.PngChunkACTL;
import ar.com.hjg.pngj.chunks.PngChunkFCTL;

import com.nostra13.universalimageloader.core.ImageLoader;

public class ApngView extends ImageView implements Animatable {
	
	private static final boolean VERBOSE = false;
	
	public static final int STATE_APNG_NULL = -1;
	public static final int STATE_APNG_READY = 0;
	public static final int STATE_APNG_RUNNING = 1;
	public static final int STATE_APNG_STOP = 2;
	
	private ArrayList<PngChunkFCTL> mFctlList;
	private ImageLoader mImageLoader;
	private String mImagePath;
	private String mWorkingPath;
	
	private int mCurrentState = STATE_APNG_NULL;
	private int mCurrentFrame;
	private int mCurrentLoop;
	
	private int mBaseWidth;
	private int mBaseHeight;
	private int mNumPlays;
	
	private float mScaling;

	public ApngView(Context context, AttributeSet attrs) {
		super(context, attrs);
		
		File workingDir = new File(context.getExternalCacheDir(), "apng");
		
		if (!workingDir.exists()) {
			workingDir.mkdirs();
		}
		
		mWorkingPath = workingDir.getPath();
		mImageLoader = ImageLoader.getInstance();
	}
	
	public void setNumPlays(int numPlays) {
		mNumPlays = numPlays;
	}
	
	@Override
	public void setImageURI(Uri uri) {
		FLog.d("ENTER");
		FLog.v("uri: %s", uri.toString());
		
		mCurrentFrame = 0;
		mCurrentLoop = 0;
		
		mCurrentState = STATE_APNG_NULL;
		
		try {
			if (uri != null) {
				String filename = uri.getLastPathSegment();
				
				File file = new File(mWorkingPath, filename);
				
				if (!file.exists()) {
					FLog.d("Copy file from %s to %s", uri.getPath(), file.getPath());
					FileUtils.copyFile(new File(uri.getPath()), file);
				}
				
				mImagePath = file.getPath();
				
				FLog.d("Extracting PNGs..");
				ApngExtractFrames.process(file);
				FLog.d("Extracting complete");
			
				FLog.d("Collect FTCL chunks");
				readApngInformation();
				
				Drawable d = Drawable.createFromPath(uri.getPath());
				setImageDrawable(d);
			
				mCurrentState = STATE_APNG_READY;
				
			} // if URI is not null
		} catch (Exception e) {
			FLog.e("Error: %s", e.toString());
		}
		
		FLog.d("EXIT");
	}
	
	@Override
	protected void onDraw(Canvas canvas) {
		FLog.d("ENTER");
		
		int width = canvas.getWidth();
		int height = canvas.getHeight();
		
		FLog.d("Canvas size: %dx%d", width, height);
		
		if (mCurrentState == STATE_APNG_STOP || mCurrentState == STATE_APNG_READY) {
			mCurrentState = STATE_APNG_READY;
			FLog.d("Reset state as READY");
			
			drawApng(canvas, false);
			
		} else if (mCurrentState == STATE_APNG_RUNNING) {
			if (mCurrentFrame >= mFctlList.size()) {
				mCurrentFrame = 0;
				
				if (mNumPlays > 0) {
					mCurrentLoop++;
				}
			}
			
			FLog.d("mNumPlays: %d, mCurrentLoop: %d, mCurrentFrame: %d", mNumPlays, mCurrentLoop, mCurrentFrame);
			
			if (mNumPlays > 0 && mCurrentLoop >= mNumPlays) {
				FLog.d("Stop");
				stop();
				
			} else if (mCurrentFrame < mFctlList.size()) {
				FLog.d("Draw frame: %d", mCurrentFrame);
				drawApng(canvas, true);
				
				mCurrentFrame++;
			}
		}
		
		FLog.d("EXIT");
	}

	@Override
	public void start() {
		FLog.d("ENTER");
		if (mCurrentState == STATE_APNG_READY) {
			mCurrentState = STATE_APNG_RUNNING;
			
			FLog.d("Invalidate");
			invalidate();
		}
		FLog.d("EXIT");
	}

	@Override
	public void stop() {
		FLog.d("ENTER");
		
		if (mCurrentState == STATE_APNG_RUNNING) {
			mCurrentState = STATE_APNG_STOP;
			mCurrentFrame = 0;
			mCurrentLoop = 0;
			
			FLog.d("Invalidate");
			invalidate();
		}
		
		FLog.d("EXIT");
	}

	@Override
	public boolean isRunning() {
		return mCurrentState == STATE_APNG_RUNNING;
	}
	
	private void readApngInformation() {
		mFctlList = new ArrayList<PngChunkFCTL>();
		
		File baseFile = new File(mImagePath);
		PngReaderApng reader = new PngReaderApng(baseFile);
		reader.end();
		
		for (PngChunk chunk : reader.getChunksList().getChunks()) {
			if (chunk instanceof PngChunkACTL) {
				mNumPlays = ((PngChunkACTL) chunk).getNumPlays();
				FLog.d("mNumPlays: %d", mNumPlays);
				
			} else if (chunk instanceof PngChunkFCTL) {
				mFctlList.add((PngChunkFCTL) chunk);
			}
		}
	}
	
	private void drawApng(Canvas canvas, boolean drawNextFrame) {
		File baseFile = new File(mImagePath);
		
		FLog.d("mCurrentFrame: %d", mCurrentFrame);
		PngChunkFCTL chunk = mFctlList.get(mCurrentFrame);
		
		int delayNum = chunk.getDelayNum();
		int delayDen = chunk.getDelayDen();
		int delay = Math.round(delayNum * 100F / delayDen);
		
		int offsetX = chunk.getxOff();
		int offsetY = chunk.getyOff();
		
		String path = new File(mWorkingPath, ApngExtractFrames.getFileName(baseFile, mCurrentFrame)).getPath();
		Bitmap bitmap = mImageLoader.loadImageSync(Uri.fromFile(new File(path)).toString());
		
		if (mCurrentFrame == 0) {
			mBaseWidth = bitmap.getWidth();
			mBaseHeight = bitmap.getHeight();
			if (VERBOSE) FLog.v("Base size: %dx%d", mBaseWidth, mBaseHeight);
			
			float scalingByWidth = ((float) canvas.getWidth())/mBaseWidth;
			if (VERBOSE) FLog.v("scalingByWidth: %.2f", scalingByWidth);
			
			float scalingByHeight = ((float) canvas.getHeight())/mBaseHeight;
			if (VERBOSE) FLog.v("scalingByHeight: %.2f", scalingByHeight);
			
			mScaling = scalingByWidth <= scalingByHeight ? scalingByWidth : scalingByHeight;
			if (VERBOSE) FLog.v("mScaling: %.2f", mScaling);
		}
		
		float dstLeft = mScaling * offsetX;
		float dstTop = mScaling * offsetY;
		float dstRight = mScaling * (offsetX + bitmap.getWidth());
		float dstBottom = mScaling * (offsetY + bitmap.getHeight());
		
		if (VERBOSE) FLog.v("DST: %.2f, %.2f, %.2f, %.2f", dstLeft, dstTop, dstRight, dstBottom);
		RectF dst = new RectF(dstLeft, dstTop, dstRight, dstBottom);
		
		canvas.drawBitmap(bitmap, null, dst, null);
		
		if (drawNextFrame) {
			FLog.d("delay: %d", delay);
			postInvalidateDelayed(delay);
		}
	}
	
	private Bitmap getBitmap() {
		File baseFile = new File(mImagePath);
		
		String path = null;
		
		Bitmap currentBitmap = null;
		Bitmap previousBitmap = null;
		Bitmap redrawnBitmap = null;
		
		Canvas canvas = null;

		int width = 0;
		int height = 0;
		
		PngChunkFCTL chunk = null;
		
		for (int i = 0; i <= mCurrentFrame; i++) {
			chunk = mFctlList.get(i);
			
			path = new File(mWorkingPath, ApngExtractFrames.getFileName(baseFile, i)).getPath();
			
			currentBitmap = BitmapUtil.getBitmap(path);
			
			byte disposeOp = ((PngChunkFCTL) chunk).getDisposeOp();
			byte blendOp = ((PngChunkFCTL) chunk).getBlendOp();
			int offsetX = ((PngChunkFCTL) chunk).getxOff();
			int offsetY = ((PngChunkFCTL) chunk).getyOff();
			
			if (width < 1 || height < 1) {
				width = currentBitmap.getWidth();
				height = currentBitmap.getHeight();
			}
			
			redrawnBitmap = redrawPng(width, height, offsetX, offsetY, blendOp, currentBitmap, previousBitmap);
			
			switch (disposeOp) {
			// APNG_DISPOSE_OP_NONE
			case PngChunkFCTL.APNG_DISPOSE_OP_NONE:
				previousBitmap = redrawnBitmap;
				break;
			// APNG_DISPOSE_OP_BACKGROUND
			case PngChunkFCTL.APNG_DISPOSE_OP_BACKGROUND:
				previousBitmap = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
				canvas = new Canvas(previousBitmap);
				canvas.drawBitmap(redrawnBitmap, 0, 0, null);
				
				canvas.clipRect(offsetX, offsetY, offsetX + currentBitmap.getWidth(), offsetY + currentBitmap.getHeight());
				canvas.drawColor(Color.TRANSPARENT, PorterDuff.Mode.CLEAR);
				canvas.clipRect(0, 0, width, height);
				break;
			// APNG_DISPOSE_OP_PREVIOUS
			case PngChunkFCTL.APNG_DISPOSE_OP_PREVIOUS:
				// forwarding previous bitmap
				break;
			}
		} // Loop through chunks
		return redrawnBitmap;
	}

	private Bitmap redrawPng(
			int width, int height, 
			int offsetX, int offsetY, byte blendOp, 
			Bitmap currentBitmap, Bitmap previousBitmap) {
		
		Bitmap redrawnBitmap = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
		
		Canvas canvas = new Canvas(redrawnBitmap);
		
		if (previousBitmap != null) {
			canvas.drawBitmap(previousBitmap, 0, 0, null);
			
			if (blendOp == PngChunkFCTL.APNG_BLEND_OP_SOURCE) {
				canvas.clipRect(offsetX, offsetY, offsetX + currentBitmap.getWidth(), offsetY + currentBitmap.getHeight());
				canvas.drawColor(Color.TRANSPARENT, PorterDuff.Mode.CLEAR);
				canvas.clipRect(0, 0, width, height);
			}
		}
		
		canvas.drawBitmap(currentBitmap, offsetX, offsetY, null);
		
		return redrawnBitmap;
	}

}
